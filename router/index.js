import express from "express";

export const router = express.Router();

export default { router };

// Configurar ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "Jaime Rodriguez Dennis // PreExamenC2" });
});

router.get("/pago", (req, res) => {
  const params = {
    titulo: "Jaime Rodriguez Dennis // PreExamenC2",
    isPost: false,
  };
  res.render("pago", params);
});

router.post("/pago", (req, res) => {
  const precios = [1.08, 2.5, 3.0];
  const { numero, nombre, domicilio, servicio, kilowatts } = req.body;
  const precio = precios[servicio - 1];
  const tipoServicio = servicio == 1 ? 'Domicilio' : servicio == 2 ? 'Comercial' : 'Industrial';
  const subtotal = precio * kilowatts;

  // Descuento
  let descuento = 0;
  if (kilowatts <= 1000) {
    descuento = 0.10;
  } else if (kilowatts > 1000 && kilowatts <= 10000) {
    descuento = 0.20;
  } else {
    descuento = 0.50;
  }
  let dtotal = subtotal * descuento;
  let totalcd = subtotal - dtotal;

  // Impuesto
  const impuesto = 0.16 * subtotal;

  // Total a pagar
  const total = (totalcd + impuesto).toFixed(2);

  const params = {
    titulo: "Jaime Rodriguez Dennis // PreExamenC2",
    numero,
    nombre,
    domicilio,
    servicio: tipoServicio,
    kilowatts,
    precio,
    subtotal,
    descuento: dtotal,
    totalcd,
    impuesto,
    total,
    isPost: true,
  };
  res.render("pago", params);
});

router.get("/contacto", (req, res) => {
    res.render("contacto", { titulo: "Jaime Rodriguez Dennis // PreExamenC2" });
});

